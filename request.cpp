#include "request.h"

#include <Windows.h>

namespace kaspersky_test {

    std::atomic_int requestCount;

    const int getterSleepTimeMs{100};
    const int processorSleepTimeMs{150};

    Request *GetRequest(Stopper stopSignal) {
        auto workEmulationTime = rand() % getterSleepTimeMs;
        if (stopSignal.wait(workEmulationTime) == WAIT_TIMEOUT)
            return new Request;
        return nullptr;
    }

    void ProcessRequest(Request *request, Stopper stopSignal) {
        assert(request);
        auto workEmulationTime = rand() % processorSleepTimeMs;
        stopSignal.wait(workEmulationTime);
    }

    void DeleteRequest(Request *request) {
        if (request != nullptr)
            delete request;
    }

}
