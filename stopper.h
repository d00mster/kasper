#pragma once

#include <memory>

#include "eventwrapper.h"

namespace kaspersky_test {

    using sync_primitives::EventWrapper;

    class Stopper {
        std::shared_ptr<EventWrapper> eventPtr_;
    public:
        Stopper() : eventPtr_(std::make_shared<EventWrapper>()) {}

        ~Stopper() {
            eventPtr_.reset();
        }

        void set() {
            eventPtr_->set();
        }

        DWORD wait(DWORD milliseconds) {
            return eventPtr_->wait(milliseconds);
        }

        const EventWrapper &getEvent() const {
            return *eventPtr_;
        }
    };

}
