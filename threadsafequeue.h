#pragma once

#include <queue>
#include <mutex> //for guards

#include "criticalsectionwrapper.h"
#include "eventwrapper.h"

namespace kaspersky_test {

    template<class T>
    class ThreadSafeQueue {
        using CSW = sync_primitives::CriticalSectionWrapper;
        mutable CSW critSec_;
        std::queue<T> queue_;
    public:
        ThreadSafeQueue(const ThreadSafeQueue &) = delete;
        ThreadSafeQueue &operator=(const ThreadSafeQueue &) = delete;

        ThreadSafeQueue() {}

        void push(T item) {
            std::lock_guard<CSW> lock(critSec_);
            queue_.push(item);
        }

        bool tryPop(T &item) {
            std::lock_guard<CSW> lock(critSec_);
            if (queue_.empty())
                return false;
            item = queue_.front();
            queue_.pop();
            return true;
        }

        size_t size() const noexcept {
            std::lock_guard<CSW> lock(critSec_);
            return queue_.size();
        }
    };

    template<class T, class D>
    void clearQueue(ThreadSafeQueue<T> &queue, D deleter) {
        T item{};
        while (queue.tryPop(item)) {
            deleter(item);
        }
    }

}
