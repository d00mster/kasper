#pragma once

#include <cassert>

#include <Windows.h>

#ifdef NDEBUG
    #define VERIFY(expression) (expression)
#else
    #define VERIFY(expression) assert(expression)
#endif

namespace sync_primitives {

    class EventWrapper {
        HANDLE handle_;
    public:
        EventWrapper(EventWrapper const &) = delete;
        EventWrapper const &operator=(EventWrapper const &) = delete;

        explicit EventWrapper(bool manualReset = true) :
            handle_(CreateEvent(nullptr, manualReset, false, nullptr)) {
            assert(handle_);
        }

        ~EventWrapper() {
            VERIFY(CloseHandle(handle_));
        }

        void set() const noexcept {
            VERIFY(SetEvent(handle_));
        }

        void reset() const noexcept {
            VERIFY(ResetEvent(handle_));
        }

        DWORD wait(DWORD milliseconds = INFINITE) const noexcept {
            return WaitForSingleObject(handle_, milliseconds);
        }

        //ф-ия ожидания сигнализации одного из несколькиз событий
        template<class... E>
        friend DWORD waitAnyEvent(const E &... events) {
            HANDLE handles[]{(events.handle_)...}; //распаковываем хендлы в массив
            return WaitForMultipleObjects(sizeof...(E), handles, FALSE, INFINITE);
        }
    };

}


