#include <ctime>

#include <algorithm>
#include <atomic>
#include <chrono>
#include <iostream>
#include <thread>

#ifndef NOMINMAX
    #define NOMINMAX
#endif
#include <Windows.h>

#include "request.h"
#include "threadsafequeue.h"

using kaspersky_test::Request;
using kaspersky_test::DeleteRequest;
using kaspersky_test::Stopper;
using sync_primitives::EventWrapper;
using RequestQueue = kaspersky_test::ThreadSafeQueue<Request*>;

std::atomic_int acceptorId;
std::atomic_int handlerId;
std::atomic_int requestsProcessed;

void doAccept(RequestQueue& queue, const EventWrapper& queueEvent, Stopper stopSignal) {
    int id{acceptorId++};
    while (true) {
        Request* request = GetRequest(stopSignal);
        //printf ("%d got request %p\n", id, request); //cout is too slow
        if (request == nullptr)
            break;
        queue.push(request);
        queueEvent.set();
    }
    printf("%d doAccept exit\n", id);
}

void doHandle(RequestQueue& queue, const EventWrapper& queueEvent, Stopper stopSignal) {
    int id{handlerId++};
    while (true) {
        Request *request{nullptr};
        try {
            auto waitResult = waitAnyEvent(queueEvent, stopSignal.getEvent());
            if (waitResult == WAIT_OBJECT_0 + 1) //stopSignal
                break;

			if (!queue.tryPop(request))
                continue;

            //printf("%d process request %p\n", id, request);
            ProcessRequest(request, stopSignal);
            ++requestsProcessed;
            DeleteRequest(request);
        }
        catch (std::exception &e) {
            std::cout << e.what() << std::endl;
            DeleteRequest(request);
        }
    }
    printf("%d doHandle exit\n", id);
}

int main() {
    std::srand(std::time(nullptr));

    Stopper stopSignal;
    RequestQueue requestQueue;
	EventWrapper queueNotEmptyEvent(false); //событие с автосбросом

    auto coresCount = std::thread::hardware_concurrency();
    size_t acceptorsThreadsCount = std::max(coresCount/2, 2u);
    size_t handlersThreadsCount = std::max(coresCount/2, 2u);
    size_t totalThreadsCount = acceptorsThreadsCount + handlersThreadsCount;

    std::vector<std::thread> threads;
    threads.reserve(totalThreadsCount);

    auto start = std::chrono::high_resolution_clock::now();

    //отправители
    for (size_t i = 0; i < acceptorsThreadsCount; ++i) {
        threads.emplace_back(doAccept, std::ref(requestQueue), std::ref(queueNotEmptyEvent), stopSignal);
    }
    //получатели
    for (size_t i = 0; i < handlersThreadsCount; ++i) {
        threads.emplace_back(doHandle, std::ref(requestQueue), std::ref(queueNotEmptyEvent), stopSignal);
    }

    Sleep(30*1000); //30 sec
    stopSignal.set();

    for (auto& thread : threads) {
        if (thread.joinable())
            thread.join();
    }

    auto elapsed = std::chrono::high_resolution_clock::now() - start;
    std::cout << "time elapsed: "
              << std::chrono::duration_cast<std::chrono::seconds>(elapsed).count()
              << " seconds"
              << std::endl;
	std::cout << "request queue size: " << requestQueue.size() << std::endl;
	std::cout << "processed request count : " << requestsProcessed << std::endl;
	std::cout << "unprocessed request count: " << kaspersky_test::requestCount << std::endl;

    //удаляем необработанные запросы
    clearQueue(requestQueue, DeleteRequest);
	std::cout << "unprocessed requests after clear: " << kaspersky_test::requestCount << std::endl;

	getc(stdin);

    return 0;
}

