#pragma once

#include <Windows.h>

namespace sync_primitives {

    class CriticalSectionWrapper {
        CRITICAL_SECTION critSec_;
    public:
        CriticalSectionWrapper(const CriticalSectionWrapper &) = delete;
        CriticalSectionWrapper &operator=(const CriticalSectionWrapper &) = delete;

        CriticalSectionWrapper() {
            InitializeCriticalSection(&critSec_);
        }

        ~CriticalSectionWrapper() {
            DeleteCriticalSection(&critSec_);
        }

        void lock() noexcept {
            EnterCriticalSection(&critSec_);
        }

        void unlock() noexcept {
            LeaveCriticalSection(&critSec_);
        }

        bool try_lock() noexcept {
            return TryEnterCriticalSection(&critSec_);
        }
    };

}
