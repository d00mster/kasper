#pragma once

#include "stopper.h"

#include <atomic>

namespace kaspersky_test {

    extern std::atomic_int requestCount;

    class Request {
    public:
        Request() { requestCount++; }
        ~Request() { requestCount--; }
    };

    // возвращает NULL, если объект stopSignal указывает на необходимость остановки,
    // либо указатель на память, которую в дальнейшем требуется удалить
    Request *GetRequest(Stopper stopSignal);

    // обрабатывает запрос, но память не удаляет, завершает обработку досрочно, если
    // объект stopSignal указывает на необходимость остановки
    void ProcessRequest(Request *request, Stopper stopSignal);

    void DeleteRequest(Request *request);

}
